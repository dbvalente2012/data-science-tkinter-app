import tkinter as tk 
import tkinter.messagebox
from tkinter import ttk 
from tkinter import *
from tkinter import scrolledtext
from tkinter.filedialog import askopenfilename, asksaveasfilename
from PIL import Image, ImageTk
from tkinter import messagebox
import csv

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objects as go

array=[]
arrayinput=[]


contas=pd.read_csv('contas.csv')
df_contas=pd.DataFrame(contas)

class tkinterApp(tk.Tk): 
      
    def __init__(self, *args, **kwargs):  
        
        tk.Tk.__init__(self, *args, **kwargs) 

        container = tk.Frame(self)   
        container.pack(side = "top", fill = "both", expand = True)  
         
        container.grid_rowconfigure(0, weight =1) 
        container.grid_columnconfigure(0, weight = 1) 
        self.frames = {}   
        for F in (StartPage,Menu,Data_science,View_cLients,Controlo_txt):
            
            frame = F(container, self) 
            self.frames[F] = frame  
            frame.grid(row = 0, column = 0, sticky ="nsew") 
        self.show_frame(StartPage) 

    def show_func_frame(self, cont): 
        frame1 = self.frames[cont]
        frame1.open_file()
        frame1.tkraise()
        
        
    def show_frame(self, cont): 
        frame = self.frames[cont]
        frame.tkraise() 

class StartPage(tk.Frame): 
    
    def __init__(self, parent, controller):  
        tk.Frame.__init__(self, parent) 
        #self.configure(bg='black')  

        
        IMAGE_PATH = 'pandas.jpg'
        WIDTH, HEIGTH = 500, 500       
        canvas = tk.Canvas(self, width=WIDTH, height=HEIGTH)
        canvas.pack()      
        img = ImageTk.PhotoImage(Image.open(IMAGE_PATH).resize((500, 500), Image.ANTIALIAS))
        canvas.background = img  # Keep a reference in case this code is put in a function.
        bg = canvas.create_image(0, 0, anchor=tk.NW, image=img)

        label = ttk.Label(self, text ="Bank Accont Management", font = ("Time", 22 ))
        label.place(x=100,y=200)
        
        
   
        button1 = ttk.Button(self, text ="Start Program",command = lambda : controller.show_frame(Menu)) 
        button1.place(x=210,y=300)
           
class Menu(tk.Frame): 
      
    def __init__(self, parent, controller): 
        

        tk.Frame.__init__(self, parent)
        
        IMAGE_PATH = 'pandas.jpg'
        WIDTH, HEIGTH = 500, 500       
        canvas = tk.Canvas(self, width=WIDTH, height=HEIGTH)
        canvas.pack()      
        img = ImageTk.PhotoImage(Image.open(IMAGE_PATH).resize((500, 500), Image.ANTIALIAS))
        canvas.background = img  # Keep a reference in case this code is put in a function.
        bg = canvas.create_image(0, 0, anchor=tk.NW, image=img)
        
        
        label = ttk.Label(self, text ="MENU", font = ("Arial", 35 )) 
        label.place(x=80, y=50)  
        
        self.configure(bg='Black')
 

        button4 = ttk.Button(self, text ="View CLients", command = lambda : controller.show_frame(View_cLients)) 
        button4.place(x=200, y=200)

        
        # button5 = ttk.Button(self, text ="Files Control", command = lambda : controller.show_frame(Controlo_txt))
        # button5.place(x=200, y=250)
        
        button3 = ttk.Button(self, text ="Data Science",command = lambda : controller.show_frame(Data_science)) 
        button3.place(x=200, y=250)

        
        button6 = ttk.Button(self, text ="Exit",command=qExit)
        button6.place(x=200, y=450)

class Data_science(tk.Frame):  
    def __init__(self, parent, controller): 
        global array

        
        linha_joao=df_contas.loc[df_contas['nome']=='João']
        tk.Frame.__init__(self, parent) 
        label = ttk.Label(self, text ="Data Science", font = ("Arial", 35 )) 
        label.place(x=120, y=50)
        name_var=tk.StringVar() 
        passw_var=tk.StringVar() 
        telef_var=tk.StringVar()
        saldo_var=tk.StringVar()

        def grafico1(): 
            a=df_contas['cidade'].value_counts()
            fig_pie = px.pie(df_contas, values=a.values, names=a.index,title='Numero clientes por cidade')
            fig_pie.show()
            
        def grafico2(): 
            
            fig_line_geo=px.line(df_contas,x=linha_joao['data_inserida'],y=linha_joao['saldo'],title='Saldo do João entre 1 a 6 de Novembro de 2020')
            fig_line_geo.update_xaxes(title_text='Data')
            fig_line_geo.update_yaxes(title_text='Saldo €')
            fig_line_geo.show()
            
        def grafico3(): 
            sort_clientes=df_contas.sort_values('saldo')
            fig_bar=px.bar(df_contas,x=sort_clientes['nome'],y=sort_clientes['saldo'],color=sort_clientes['data_inserida'],labels={'saldo':'População'},title='Saldo de contas dos clientes')
            fig_bar.update_xaxes(title_text='Clientes')
            fig_bar.update_yaxes(title_text='Saldo €')
            fig_bar.show()
            
            
        saldo_maior=df_contas.loc[df_contas['saldo'].idxmax,['nome']]
        for val in saldo_maior:
            maximo=val
        name_label = tk.Label(self, text = 'Client with the higher bank balance: ', font=('arial',10, 'bold'))
        name_label.place(x=30, y=150)
        name_entry = tk.Entry(self, textvariable = name_var,font=('calibre',10,'normal')) 
        name_entry.place(x=280, y=150)

        saldo_menor=df_contas.loc[df_contas['saldo'].idxmin,['nome']]
        for val1 in saldo_menor:
            minimo=val1
        passw_label = tk.Label(self,text = 'Client with the lowest bank balance: ', font = ('arial',10,'bold'))
        passw_label.place(x=30, y=180)
        passw_entry=tk.Entry(self, textvariable = passw_var,font = ('calibre',10,'normal')) 
        passw_entry.place(x=280, y=180)
        

        num_total_operacoes=len(df_contas)
        saldo_label = tk.Label(self,text = 'Operations number:  ', font = ('calibre',10,'bold')) 
        saldo_label.place(x=30, y=240)
        saldo_entry=tk.Entry(self, textvariable = saldo_var,font = ('calibre',10,'normal')) 
        saldo_entry.place(x=280, y=240)
        
        df_contas.drop_duplicates(subset ='contribuinte',inplace=True)
        num_total_clientes=len(df_contas)
        telef_label = tk.Label(self,text = 'Number of clients: ', font = ('arial',10,'bold')) 
        telef_label.place(x=30, y=210)
        telef_entry=tk.Entry(self, textvariable = telef_var,font = ('calibre',10,'normal')) 
        telef_entry.place(x=280, y=210)
       
        
        prim_graf=tk.Button(self,text = 'Graph - Client count by city',command = grafico1) 
        prim_graf.place(x=150, y=300)
        
        seg_graf=tk.Button(self,text = 'Graph - João client balance variation',command = grafico2) 
        seg_graf.place(x=150, y=350)
        
        terc_graf=tk.Button(self,text = 'Graph - Value of clients balances',command = grafico3) 
        terc_graf.place(x=150, y=400)
           
        name_entry.insert(END,maximo)
        passw_entry.insert(END,minimo)
        telef_entry.insert(END,num_total_clientes)
        saldo_entry.insert(END,num_total_operacoes)
        
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)
        

class Controlo_txt(tk.Frame):  
    def __init__(self, parent, controller): 
        global array
        global arrayinput
        global df_contas
        tk.Frame.__init__(self, parent) 
        label = ttk.Label(self, text ="Files Control", font = ("Arial", 35 )) 
        label.place(x=130, y=50)
        
        
        
        def save_file():
            filepath = asksaveasfilename(
                defaultextension="csv",
                filetypes=[("Text Files", "*.csv")],
            )
            if not filepath:
                return              
            with open(filepath, 'w',newline='') as csvfile:          
                    x = csv.writer(csvfile, delimiter='\t') 
                    for escreve_linha_csv in array:
                        x.writerow(escreve_linha_csv)
                    tkinter.messagebox.showinfo(title='Info', message='You just EXPORT your information to a CSV file')
        
      
                
        def open_file():
            filepath = askopenfilename(
                filetypes=[("Text Files", "*.csv")]
            )
            if not filepath:
                return
            with open(filepath, "r") as csvfile:
                le_array_csv = csv.reader(csvfile, delimiter='\t')
                linhas_no_csv=len(list(csv.reader(open(filepath))))
                if linhas_no_csv>=1:
                    for linha_cliente in le_array_csv:
                        print(linha_cliente) 
                        array.append(linha_cliente)

                    tkinter.messagebox.showinfo(title='Info', message='Acabou de fazer IMPORT da sua informação de um ficheiro CSV')
    
        btn_open = ttk.Button(self, text="Open", command=open_file)
        btn_open.place(x=200, y=200)
        btn_save = ttk.Button(self, text="Save As", command=save_file)
        btn_save.place(x=200, y=300)
        
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)
              
class View_cLients(tk.Frame):  
    def __init__(self, parent, controller): 
        global array
        global arrayinput
        print(array)
        df_contas=pd.DataFrame(contas)
        tk.Frame.__init__(self, parent) 
        label = ttk.Label(self, text ="View Clients", font = ("Arial", 20 )) 
        label.place(x=180, y=5)
      
        txt_edit=Text(self,width=60,height=22,bg='white',bd=1) 
        #txt_edit.configure()      
        txt_edit.place(x=10, y=50)
        
        def visualizar():
            txt_edit.delete('1.0',END)
            txt_edit.insert(END,df_contas)
    
 
        button2=ttk.Button(self, text ="Update/View Database", command = visualizar)
        button2.place(x=50, y=450)        
        button1=ttk.Button(self, text ="Menu", command = lambda : controller.show_frame(Menu))
        button1.place(x=400, y=450)
        
def qExit():
    qExit=tkinter.messagebox.askyesno('Quit System','Do you want to Quit')
    
    if qExit> 0:
        app.destroy()
        return
        
if __name__ == "__main__":
    
    app = tkinterApp()
    app.title("Bank Accont Management - Data Science")
    app.geometry('500x500')
    app.resizable(width=False, height=False) 
    
    app.mainloop()



